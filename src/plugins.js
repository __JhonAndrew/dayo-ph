import firebase from 'firebase'
import moment from 'moment'

export default function install (Vue, options) {
  // Database Handler
  Vue.prototype.$database = firebase.database()

  // Current User
  Vue.prototype.$currentUser = () => {
    return firebase.auth().currentUser
  }

  // Input Rules
  Vue.prototype.$rules = {
    required: v => !!v || 'This field is required.',
    /* eslint-disable no-useless-escape */
    email: v => /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(v) || 'E-mail must be valid'
  }

  // Time Parser
  Vue.prototype.$timestamp = (time, format = 'calendar') => {
    switch (format) {
      case 'fromNow':
        return moment.unix(time).fromNow()
      case 'calendar':
        return moment.unix(time).calendar()
      default:
        return moment.unix(time).format(format)
    }
  }
}
