import Vue from 'vue'
import Vuetify from 'vuetify'
import router from './router'
import App from './index'
import Plugins from './plugins'
import store from './state'
import firebase from 'firebase'

// Vuetify
Vue.use(Vuetify)
require('material-design-icons-iconfont/dist/material-design-icons.css')
require('roboto-fontface/css/roboto/roboto-fontface.css')
// require('vuetify/dist/vuetify.min.css')

// Firebase
firebase.initializeApp(store.state.firebase)

// Route Guards
router.beforeEach((to, from, next) => {
  let {requiresAuth} = to.meta
  // Route Validator
  let validateRoute = () => {
    if ((requiresAuth && !store.state.authenticated)) {
      router.push({name: 'login', params: {ref: to.fullPath}})
    } else next()
  }

  // Check auth state if not yet observing
  if (!store.state.observingAuthState) {
    // Firebase Auth Observer
    firebase.auth().onAuthStateChanged(user => {
      store.commit('setState', {
        authenticated: !!user,
        observingAuthState: true
      })

      if (!user && store.state.user) {
        store.commit('setState', {user: null})
        router.push({name: 'login', params: {ref: to.fullPath}})
      } else validateRoute()
    })
  } else validateRoute()

  // console.log({from}, {to})
})

// Vue Configurations
Vue.config.productionTip = false
Vue.config.devtools = false

// Custom Plugins
Vue.use(Plugins)

/* eslint-disable no-new */
new Vue({
  el: 'app-root',
  store,
  router,
  template: '<app />',
  components: { App },
  // for debugging only
  created () {
    window.firebase = firebase
    window.$router = this.$router
  }
})
