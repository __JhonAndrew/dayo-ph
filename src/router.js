import Vue from 'vue'
import Router from 'vue-router'

// App Views
import Login from '@/views/login'
import Signup from '@/views/signup'
import Users from '@/views/users'
import User from '@/views/user'
import Games from '@/views/games'
import Game from '@/views/game'
import Merchants from '@/views/merchants'
import Merchant from '@/views/merchant'

Vue.use(Router)

// Route Configurations
export default new Router({
  mode: 'history',
  scrollBehavior (to, from, savedPosition) {
    return savedPosition || {x: 0, y: 0}
  },
  routes: [
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/signup',
      name: 'signup',
      component: Signup
    },
    {
      path: '/users',
      alias: '/',
      name: 'users',
      component: Users,
      meta: {requiresAuth: true}
    },
    {
      path: '/user/:id',
      name: 'user',
      component: User,
      meta: {requiresAuth: true}
    },
    {
      path: '/games',
      name: 'games',
      component: Games,
      meta: {requiresAuth: true}
    },
    {
      path: '/game/:id',
      name: 'game',
      component: Game,
      meta: {requiresAuth: true}
    },
    {
      path: '/merchants',
      name: 'merchants',
      component: Merchants,
      meta: {requiresAuth: true}
    },
    {
      path: '/merchant/:id',
      name: 'merchant',
      component: Merchant,
      meta: {requiresAuth: true}
    }
  ]
})
