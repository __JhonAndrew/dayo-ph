import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    firebase: {
      apiKey: 'AIzaSyCPSS0aC07083oczI2LmrETq3q5P5GvS3A',
      authDomain: 'dayo-app-a9b37.firebaseapp.com',
      databaseURL: 'https://dayo-app-a9b37.firebaseio.com',
      projectId: 'dayo-app-a9b37',
      storageBucket: 'dayo-app-a9b37.appspot.com',
      messagingSenderId: '1010068521147'
    },
    authenticated: false,
    user: null,
    observingAuthState: false,
    gameTypes: [
      { text: '3 vs 3', value: '3vs3' },
      { text: '5 vs 5', value: '5vs5' },
      { text: '3-point-shootout', value: '3ps' }
    ],
    gameStatus: [
      { text: 'Waiting', value: 'waiting' },
      { text: 'Full', value: 'full' },
      { text: 'Playing', value: 'playing' },
      { text: 'Completed', value: 'completed' }
    ]
  },
  mutations: {
    setState (state, props) {
      Object.keys(props).forEach(prop => {
        state[prop] = props[prop]
      })
    },
    login (state, user) {
      state.user = user
      state.authenticated = true
    }
  }
})
